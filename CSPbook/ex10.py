# setup the source list
source = ["This","is","a","list"]

# Set the accumulator to the empty list
so_far = []

# Loop through all the items in the source list
for index in source:

    # Add a list with the current item from source to so_far
    so_far =  [index] + so_far
print(so_far)
