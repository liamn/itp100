# initialize the variables
source = ["This", "is", "a", "list"]
new_list = []

# loop from the last index to the first (0)
for index in range(len(source), 1, -1):
    new_list = [index] + new_list

# print the current value of the list
print(new_list)
