def function(my_list):
    part1 = 0
    part2 = 0
    for i in my_list:
        if i > 0:
            part1 += i
        else:
            part2 += abs(i)
    return part1 + part2


print(function([3, 5, 2, -23, 5]))
