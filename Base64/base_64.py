def to_base(num, base):
    """
    Convert an decimal integer into a string representation of the digits
    representing the number in the base (between 2 and 16) provided.

      >>> to_base(10, 3)
      '101'
      >>> to_base(11, 2)
      '1011'
      >>> to_base(10, 6)
      '14'
      >>> to_base(21, 3)
      '210'
      >>> to_base(21, 11)
      '1A'
      >>> to_base(47, 16)
      '2F'
      >>> to_base(65535, 16)
      'FFFF'
    """
    if num == 0:
        return '0'

    if base == 64:
        return base64(num)

    new_num = ''
    while num > 0:
        new_digit = num % base
        if new_digit < 10:
            new_num = str(new_digit) + new_num
        else:
            new_num = chr(new_digit + 55) + new_num
        num //= base
    return new_num


def base64(num):
    index = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    new_num = ''
    while num > 0:
        new_digit = num % 64
        new_num += index[new_digit]
        num //= 64
    return new_num

def base256(num):
    new_num = ''
    while num > 0:
        new_digit = num % 256
        new_num += chr(new_digit)
        num //= 64
    return new_num


def fromASCIIto64(text):
    '''
        >>> fromASCIIto64('This is a test.')
        'VGhpcyBpcyBhIHRlc3Qu'
    '''

    stuff = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

    binary = ''
    for i in text:
        instance = bin(ord(i))
        instance = instance[2:]
        instance = '0'*(8 - len(instance)) + instance
        binary += instance
    while len(binary) % 24 != 0:
        binary += '0'

    result = ''
    i = 0
    while i < len(binary) / 6:
        j = [int(binary[i*6 + k]) for k in range(6)]
        order = 0
        for k in range(6):
            order += (2 ** k) * j[5 - k]
        result += stuff[order]
        i += 1
    return result


def from64toASCII(text):
    '''
        >>> from64toASCII('VGhpcyBpcyBhIHRlc3Qu')
        'This is a test.'
    '''

    stuff = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

    binary = ''
    for i in text:
        instance = bin(stuff.index(i))
        instance = instance[2:]
        instance = '0' * (6 - len(instance)) + instance
        binary += instance
    while len(binary) % 24 != 0:
        binary += '0'

    result = ''
    i = 0
    while i < len(binary) / 8:
        j = [int(binary[i * 8 + k]) for k in range(8)]
        order = 0
        for k in range(8):
            order += (2 ** k) * j[7 - k]
        result += chr(order)
        i += 1
    return result

def base64encode(three_bytes):
    """
      >>> base64encode(b'\\x5A\\x2B\\xE6')
      'Wivm'
      >>> base64encode(b'\\x49\\x33\\x8F')
      'STOP'
      >>> base64encode(b'\\xFF\\xFF\\xFF')
      '////'
      >>> base64encode(b'\\x00\\x00\\x00')
      'AAAA'
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='

    try:
        # turn the three bytes into ints
        b1 = three_bytes[0]
        index1 = b1 >> 2

        if len(three_bytes) > 1:
            b2 = three_bytes[1]
            index2 = (b1 & 3) << 4 | b2 >> 4
        else:
            index2 = 64
            
        if len(three_bytes) > 2:
            b3 = three_bytes[2]
            index3 = (b2 & 15) << 2 | (b3 & 192) >> 6
            index4 = b3 & 63
        else:
            index3 = 64
            index4 = 64
        
    except (AttributeError, TypeError, IndexError):
        raise AssertionError('Input should be 3 bytes')

    return f'{digits[index1]}{digits[index2]}{digits[index3]}{digits[index4]}'


def base64decode(four_bytes):
    '''
    '''

    #digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    #hexd = '0123456789ABCDEF'
    
    b = four_bytes

    if len(b) > 1:
        index1 = b[0] << 2 | b[1] >> 4
    else:
        index1 = None
    if len(b) > 2:
        index2 = (b[1] & 15) << 4 | (b[2] >> 2)
    else:
        index2 = None
    if len(b) > 3:
        index3 = (b[2] & 3) << 6 | b[3]
    else:
        index3 = None
    
    result = b''
    if index1 != None:
        result += bytes([index1 & 255])
    if index2 != None:
        result += bytes([index2 & 255])
    if index3 != None:
        result += bytes([index3 & 255])
    return result

def imagetobase64(imagename):
    import os
    image_file = open(imagename, "rb")
    encoded = ''
    file_size = os.path.getsize(imagename)
    for i in range(file_size // 3 + 1):
        data = image_file.read(3)
        encoded += base64encode(data)
    image_file.close()
    print(encoded)
    text_file = open("encoded.txt","w+")
    text_file.write(encoded) 
    text_file.close()

def base64toimage(textname):
    import os
    text_file = open(textname, "rb")
    decoded = b''
    file_size = os.path.getsize(textname)
    for i in range(file_size // 4 + 1):
        data = text_file.read(4)
        decoded += base64decode(data)
    text_file.close()
    print(decoded)
    image_file = open("decoded.txt","w+")
    image_file.write(encoded)
    image_file.close()


imagetobase64('test.png')
base64toimage('encoded.txt')
