import math

thefile = open('alice_in_wonderland.txt')
text = thefile.read()
thefile.close()
counter = [0]*128

for i in text:
    counter[ord(i)] += 1


print('ALICE IN WONDERLAND HISTOGRAM')
for j in range(32, 128):
    print(str(chr(j)) + ' ' + str(counter[j]) + ' ' + ('█' * math.ceil(counter[j] / 250)))
