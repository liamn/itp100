thefile = open('unsorted_fruits_with_repeats.txt', 'r')
unsorted = thefile.readlines()
thefile.close()
sorteds = sorted(unsorted)

unique = 0
newnames = ['']
counter = [0]
for i in range(1, len(sorteds)):
    if sorteds[i - 1] == sorteds[i]:
        counter[unique] += 1
    else:
        counter.append(0)
        unique += 1
        counter[unique] = 1
        newnames.append(sorteds[i])

stringsorted = ''
up = 0
for i in range(unique):
    if up - 1:
        stringsorted += str(counter[up]) + ' '
    stringsorted += newnames[i]
    up += 1

thenewfile = open('sorted_fruits_with_repeats.txt', 'w')
thenewfile.write(stringsorted)
thenewfile.close()
print(open('sorted_fruits_with_repeats.txt', 'r').read())
